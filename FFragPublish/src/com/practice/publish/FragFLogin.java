package com.practice.publish;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.LoggingBehavior;
import com.facebook.Request;
import com.facebook.RequestAsyncTask;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Settings;


import com.practice.ffraglogin.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class FragFLogin extends Fragment {
	   private static final String URL_PREFIX_FRIENDS = "https://graph.facebook.com/me/friends?access_token=";

	    private TextView txvLoginState;
	    private Button btnLoginLogout;
	    private Button btnPublish;
	    private Session.StatusCallback statusCallback = new SessionStatusCallback();
	    
	    
	    private static final List<String> PERMISSIONS = Arrays.asList("publish_actions");
	    private static final String PENDING_PUBLISH_KEY = "pendingPublishReauthorization";
	    private boolean pendingPublishReauthorization = false;

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        View view = inflater.inflate(R.layout.frag_flogin, container, false);

	        btnLoginLogout = (Button) view.findViewById(R.id.btnFragLoginLoginLogout);
	        txvLoginState = (TextView) view.findViewById(R.id.txvFragloginLoginState);
	        btnPublish = (Button)view.findViewById(R.id.btnFragloginPublish);
	        
	        btnPublish.setOnClickListener(new View.OnClickListener() {
	            @Override
	            public void onClick(View v) {
	                publishStory();        
	            }
	        });

	        Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

	        Session session = Session.getActiveSession();
	        if (session == null) {
	            if (savedInstanceState != null) {
	                session = Session.restoreSession(getActivity(), null, statusCallback, savedInstanceState);
	            }
	            if (session == null) {
	                session = new Session(getActivity());
	            }
	            Session.setActiveSession(session);
	            if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED)) {
	                session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
	            }
	        }

	        updateView();
	        
	        if (savedInstanceState != null) {
	            pendingPublishReauthorization = 
	                savedInstanceState.getBoolean(PENDING_PUBLISH_KEY, false);
	        }

	        return view;
	    }

	    @Override
	    public void onStart() {
	        super.onStart();
	        Session.getActiveSession().addCallback(statusCallback);
	    }

	    @Override
	    public void onStop() {
	        super.onStop();
	        Session.getActiveSession().removeCallback(statusCallback);
	    }

	    @Override
	    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
	    }

	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        Session session = Session.getActiveSession();
	        outState.putBoolean(PENDING_PUBLISH_KEY, pendingPublishReauthorization);
	        Session.saveSession(session, outState);
	        


	    }

	    private void updateView() {
	        Session session = Session.getActiveSession();
	        if (session.isOpened()) {
	            txvLoginState.setText(URL_PREFIX_FRIENDS + session.getAccessToken());
	            btnLoginLogout.setText("logout");
	            btnLoginLogout.setOnClickListener(new View.OnClickListener() {
	                public void onClick(View view) { onClickLogout(); }
	            });
	        } else {
	            txvLoginState.setText("Now, Logout FB!!");
	            btnLoginLogout.setText("login");
	            btnLoginLogout.setOnClickListener(new View.OnClickListener() {
	                public void onClick(View view) { onClickLogin(); }
	            });
	        }
	    }

	    private void onClickLogin() {
	        Session session = Session.getActiveSession();
	        if (!session.isOpened() && !session.isClosed()) {
	            session.openForRead(new Session.OpenRequest(this).setCallback(statusCallback));
	        } else {
	            Session.openActiveSession(getActivity(), this, true, statusCallback);
	        }
	    }

	    private void onClickLogout() {
	        Session session = Session.getActiveSession();
	        if (!session.isClosed()) {
	            session.closeAndClearTokenInformation();
	        }
	    }

	    private class SessionStatusCallback implements Session.StatusCallback {
	        @Override
	        public void call(Session session, SessionState state, Exception exception) {
	            updateView();
	        }
	    }
	    
	    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
	        if (state.isOpened()) {
	        	btnPublish.setVisibility(View.VISIBLE);
	        	if (pendingPublishReauthorization && 
	        	        state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
	        	    pendingPublishReauthorization = false;
	        	    publishStory();
	        	}
	        } else if (state.isClosed()) {
	        	btnPublish.setVisibility(View.INVISIBLE);
	        }
	    }
	    
	    
	    private void publishStory() {
	        Session session = Session.getActiveSession();

	        if (session != null){

	            // Check for publish permissions    
	            List<String> permissions = session.getPermissions();
	            if (!isSubsetOf(PERMISSIONS, permissions)) {
	                pendingPublishReauthorization = true;
	                Session.NewPermissionsRequest newPermissionsRequest = new Session
	                        .NewPermissionsRequest(this, PERMISSIONS);
	            session.requestNewPublishPermissions(newPermissionsRequest);
	                return;
	            }

	            Bundle postParams = new Bundle();
	            postParams.putString("name", "Facebook SDK for Android");
	            postParams.putString("caption", "Build great social apps and get more installs.");
	            postParams.putString("description", "The Facebook SDK for Android makes it easier and faster to develop Facebook integrated Android apps.");
	            postParams.putString("link", "https://developers.facebook.com/android");
	            postParams.putString("picture", "https://raw.github.com/fbsamples/ios-3.x-howtos/master/Images/iossdk_logo.png");

	            Request.Callback callback= new Request.Callback() {
	                public void onCompleted(Response response) {
	                    JSONObject graphResponse = response
	                                               .getGraphObject()
	                                               .getInnerJSONObject();
	                    String postId = null;
	                    try {
	                        postId = graphResponse.getString("id");
	                    } catch (JSONException e) {
	                        Log.i("Bug",
	                            "JSON error "+ e.getMessage());
	                    }
	                    FacebookRequestError error = response.getError();
	                    if (error != null) {
	                        Toast.makeText(getActivity()
	                             .getApplicationContext(),
	                             error.getErrorMessage(),
	                             Toast.LENGTH_SHORT).show();
	                        } else {
	                            Toast.makeText(getActivity()
	                                 .getApplicationContext(), 
	                                 postId,
	                                 Toast.LENGTH_LONG).show();
	                    }
	                }
	            };

	            Request request = new Request(session, "me/feed", postParams, 
	                                  HttpMethod.POST, callback);

	            RequestAsyncTask task = new RequestAsyncTask(request);
	            task.execute();
	        }

	    }
	    
	    private boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
	        for (String string : subset) {
	            if (!superset.contains(string)) {
	                return false;
	            }
	        }
	        return true;
	    }

}
