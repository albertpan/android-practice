package com.practice.publish;


import com.facebook.Session;
import com.facebook.SessionState;



import com.practice.ffraglogin.R;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class FragFPublish extends Fragment {

	    private TextView txvLoginState;
	    private Button btnPublish;
	    private EditText edtShow;
	    private Session.StatusCallback statusCallback = new SessionStatusCallback();

	    @Override
	    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
	        View view = inflater.inflate(R.layout.frag_fpubish, container, false);

	        btnPublish = (Button) view.findViewById(R.id.btnFragPublishPublish);
	        txvLoginState = (TextView) view.findViewById(R.id.txvFragPublishLoginState);
	        edtShow = (EditText)view.findViewById(R.id.edtFragPublishShowText);
	        



	        updateView();

	        return view;
	    }

	    @Override
	    public void onStart() {
	        super.onStart();
	        Session.getActiveSession().addCallback(statusCallback);
	    }

	    @Override
	    public void onStop() {
	        super.onStop();
	        Session.getActiveSession().removeCallback(statusCallback);
	    }

	    @Override
	    public void onActivityResult(int requestCode, int resultCode, Intent data) {
	        super.onActivityResult(requestCode, resultCode, data);
	        Session.getActiveSession().onActivityResult(getActivity(), requestCode, resultCode, data);
	    }

	    @Override
	    public void onSaveInstanceState(Bundle outState) {
	        super.onSaveInstanceState(outState);
	        Session session = Session.getActiveSession();
	        Session.saveSession(session, outState);
	    }

	    private void updateView() {
	        Session session = Session.getActiveSession();
	        if (session.isOpened()) {
	            txvLoginState.setText("Now, Login FB");


	        } else {
	            txvLoginState.setText("Now, logout!!");


	        }
	    }



	    private class SessionStatusCallback implements Session.StatusCallback {
	        @Override
	        public void call(Session session, SessionState state, Exception exception) {
	            updateView();
	        }
	    }

}
